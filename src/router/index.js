import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@/views/HomePage";
import SignUpPage from "@/views/SignUpPage";
const routes = [
  {
    path: "/",
    name: "home",
    component: HomePage,
  },
  {
    path: "/sign-up",
    name: "sign-up",
    component: SignUpPage,
    meta: {
      NavigationComponent: false,
      FooterComponent: false,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
